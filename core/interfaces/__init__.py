from .storage import IUsers as IUsersStorage
from .storage import IShop as IShopStorage
from .storage import IShopInfo as IShopInfoStorage
from .storage import IShopImage as IShopImageStorage
from .storage import IStorage

__all__ = [
    'IStorage', 'IUsersStorage', 'IShopStorage',
    'IShopInfoStorage', 'IShopImageStorage'
]
