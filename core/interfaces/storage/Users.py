from typing import Iterable
from core.models import MUser


class Users():
    def get_by_id(self, _id: int) -> MUser | None:
        pass

    def get_all(self, limit: int = 0, offset: int = 0) -> Iterable[MUser]:
        pass
