from typing import Iterable
from core.models import MShop
from core.models import MShopInfo


class Shop:
    def get_by_id(self, _id: int) -> MShop | None:
        pass

    def get_all(self, limit: int = 0, offset: int = 0) -> Iterable[MShop]:
        pass


