from typing import List
from core.models import MShopImage


class ShopImage():
    def get_by_id(self, _id: int) -> MShopImage | None:
        pass

    def get_by_item_id(self, item_id: int) -> List[MShopImage] | None:
        pass

    def get_all(self, limit: int = 0, offset: int = 0) -> List[MShopImage]:
        pass
