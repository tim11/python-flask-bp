from typing import Iterable
from core.models import MShopInfo


class ShopInfo():
    def get_by_id(self, _id: int) -> MShopInfo | None:
        pass

    def get_by_item_id(self, item_id: int) -> MShopInfo | None:
        pass

    def get_all(self, limit: int = 0, offset: int = 0) -> Iterable[MShopInfo]:
        pass
