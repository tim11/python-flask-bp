from abc import ABC
from .Users import Users


class Storage(ABC):
    __users__ = None

    @property
    def users(self) -> Users:
        return self.__users__

    @users.setter
    def users(self, value: Users):
        self.__users__ = value

