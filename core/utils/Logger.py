import sys

from loguru import logger
from pathlib import PurePath
from .Configurator import Configurator


class Logger:
    __config__ = {
        'backtrace': True,
        'diagnose': True,
        'format': '{time} [{level}] {message}'
    }
    debug = logger.debug
    info = logger.info
    warning = logger.warning
    error = logger.error
    exception = logger.opt(exception=True).critical

    @staticmethod
    def config(config: Configurator):
        if config.get('logger.console'):
            logger.remove()
            console_cf = Logger.__config__.copy()
            format_cf = config.get('logger.console.format')
            if format_cf:
                console_cf['format'] = format_cf
            console_cf['colorize'] = True
            logger.add(
                sys.stdout,
                **console_cf,
                level=config.get('logger.console.level', 'DEBUG')
            )
        if config.get('logger.file'):
            path = config.get('logger.file.path')
            if not path:
                return
            file_cf = Logger.__config__.copy()
            format_cf = config.get('logger.file.format')
            if format_cf:
                file_cf['format'] = format_cf
            file_cf['rotation'] = config.get('logger.file.rotation')
            logger.add(
                PurePath(path, 'app.log'),
                **file_cf,
                filter=lambda record: record['level'].name in ['DEBUG', 'INFO', 'WARNING']
            )
            logger.add(
                PurePath(path, 'error.log'),
                **file_cf,
                filter=lambda record: record['level'].name in ['ERROR', 'CRITICAL']
            )

