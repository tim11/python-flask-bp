from dynaconf import Dynaconf


class Configurator:
    __config__: Dynaconf = None

    def __init__(self, config_file: str):
        self.__config__ = Dynaconf(
            settings_file=[config_file],
            environments=False
        )

    def get(self, key_string, default=None):
        try:
            return self.__config__[key_string]
        except BaseException:
            return default
