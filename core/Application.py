from abc import ABC, abstractmethod
from .storages import DbStorage
from .utils import Configurator, Logger


class Application(ABC):
    __storage__: DbStorage = None
    __config__: Configurator = None
    __logger__ = None

    def __init__(self, config_file: str):
        self.__config__ = Configurator(config_file)
        self.__logger__ = Logger.config(self.__config__)
        try:
            self.__storage__ = DbStorage(self.__config__)
        except Exception as e:
            self.__logger__.exception(e)
            raise e

    @property
    def logger(self):
        return self.__logger__

    @property
    def config(self):
        return self.__config__

    @property
    def storage(self):
        return self.__storage__

    @abstractmethod
    def run(self):
        pass
