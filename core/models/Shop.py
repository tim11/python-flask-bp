from dataclasses import dataclass
from typing import List
from core.models.ShopInfo import ShopInfo
from core.models.ShopImage import ShopImage


@dataclass
class Shop:
    id: int
    name: str
    images: List[ShopImage]
    info: ShopInfo | None


if __name__ == '__main__':
    shop = Shop(id=1, name='Box with papers', info=None, images=[])
    shop.info = ShopInfo(id=1, item_id=1, description='Little Box')
    shop.images.extend([
        ShopImage(id=1, item_id=1, src='https://sc02.alicdn.com/kf/HTB1u6hjasfrK1RkSnb4q6xHRFXaE/Supply-custom-printed-cardboard-portable-office-file.jpg'),
        ShopImage(id=1, item_id=1, src='https://officepage.ru/upload/iblock/e6f/ilhsz1lehd3ux0foflvkquxspednb3tr/qt5h29wgxhlc7huwh0u1i17hoq9vg345.jpg')
    ])
    assert shop.id == 1, 'Неверное ID товара'
    assert shop.name == 'Box with papers', 'Неверное название товара'
    assert shop.info.id == 1, 'Неверное ID описания товара'
    assert shop.info.item_id == 1, 'Неверное ID товара'
    assert shop.info.description == 'Little Box', 'Неверное описание товарар'
    assert len(shop.images) == 2, 'Неверное количество изображений'
