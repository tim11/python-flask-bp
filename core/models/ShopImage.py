from dataclasses import dataclass


@dataclass
class ShopImage:
    id: int
    item_id: int
    src: str

    def __str__(self):
        return self.src


if __name__ == '__main__':
    image = 'https://sc02.alicdn.com/kf/HTB1u6hjasfrK1RkSnb4q6xHRFXaE/Supply-custom-printed-cardboard-portable-office-file.jpg'
    shop_image = ShopImage(id=1, item_id=1, src=image)
    assert shop_image.id == 1, 'Неверное ID картинки товара'
    assert shop_image.item_id == 1, 'Неверное ID товара'
    assert shop_image.src == image, 'Неверная ссылка товара'
