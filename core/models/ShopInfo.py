from dataclasses import dataclass


@dataclass
class ShopInfo:
    id: int
    item_id: int
    description: str


if __name__ == '__main__':
    shop_info = ShopInfo(id=1, item_id=1, description='Little Box')
    assert shop_info.id == 1, 'Неверное ID описания товара'
    assert shop_info.item_id == 1, 'Неверное ID товара'
    assert shop_info.description == 'Little Box', 'Неверное описание товара'
