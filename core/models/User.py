from dataclasses import dataclass


@dataclass
class User:
    id: int
    name: str


if __name__ == '__main__':
    user = User(id=1, name='Valera2')
    assert user.id == 1, 'Неверное ID пользователя'
    assert user.name == 'Valera', 'Неверное имя пользователя'
