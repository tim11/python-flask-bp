import csv

from core.interfaces import IUsersStorage
from core.models import MUser


class Users(IUsersStorage):
    __filename__ = None
    __data__ = []

    def __init__(self, file_name):
        self.__filename__ = file_name
        with open(file_name, newline='') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';')
            for row in reader:
                self.__data__.append(
                    MUser(
                        id=int(row.get('id')),
                        name=row.get('name')
                    )
                )

    def get_by_id(self, _id: int) -> MUser | None:
        result = list(filter(lambda user: user.id == id, self.__data__))
        if len(result):
            return result[0]

    def get_all(self, limit: int = 0, offset: int = 0):
        return self.__data__
