import pathlib
from core.interfaces import IStorage
from .Users import Users


class Storage(IStorage):
    def __init__(self, path):
        super().__init__()
        self.users = Users(
            pathlib.PurePath(path, 'users.csv')
        )
