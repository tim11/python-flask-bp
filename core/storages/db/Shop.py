from typing import Iterable, Dict

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, PrimaryKeyConstraint
from core.interfaces import IShopStorage
from core.models import MShop, MShopInfo

from .Session import Session


Base = declarative_base()


class Shop(Base, IShopStorage):
    __tablename__ = 'shop'
    id = Column(Integer(), comment='Идентификатор', autoincrement=True)
    name = Column(String(100), nullable=False, comment='Название товара')

    __table_args__ = (
        PrimaryKeyConstraint('id', name='shop_pk'),
    )

    __cached = Dict

    def __init__(self, session: Session):
        Base.__init__(self)
        IShopStorage.__init__(self)
        self.session = session

    def get_all(self, limit: int = 0, offset: int = 0) -> Iterable[MShop]:
        from .ShopInfo import ShopInfo
        from .ShopImage import ShopImage
        result = []
        query = self.session.instance.query(self.__class__)
        if limit:
            query = query.limit(limit)
        if offset:
            query = query.offset(offset)
        items = query.all()
        for item in items:
            result.append(
                MShop(
                    id=item.id,
                    name=item.name,
                    info=ShopInfo(self.session).get_by_item_id(item.id),
                    images=ShopImage(self.session).get_by_item_id(item.id)
                )
            )
        return result

    def get_by_id(self, _id: int) -> MShop | None:
        from .ShopInfo import ShopInfo
        from .ShopImage import ShopImage
        item = self.session.instance.query(self.__class__).get(_id)
        if not item:
            return None
        return MShop(
            id=item.id,
            name=item.name,
            info=ShopInfo(self.session).get_by_item_id(item.id),
            images=ShopImage(self.session).get_by_item_id(item.id)
        )

