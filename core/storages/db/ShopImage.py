from typing import List

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, PrimaryKeyConstraint, String, ForeignKey

from core.interfaces import IShopImageStorage
from core.models import MShopImage
from .Session import Session
from .Shop import Shop

Base = declarative_base()


class ShopImage(Base, IShopImageStorage):
    __tablename__ = 'shop_images'
    id = Column(Integer(), comment='Идентификатор', autoincrement=True)
    item_id = Column(Integer(), ForeignKey(
                         column=Shop.id,
                         onupdate='CASCADE',
                         ondelete='CASCADE'
                     ), comment='Идентификатор товар', nullable=False)
    src = Column(String(255), nullable=False, comment='Ссылка на картинку')

    __table_args__ = (
        PrimaryKeyConstraint('id', name='shop_image_pk'),
    )

    def __init__(self, session: Session):
        Base.__init__(self)
        IShopImageStorage.__init__(self)
        self.session = session

    def get_all(self, limit: int = 0, offset: int = 0) -> List[MShopImage]:
        result = []
        query = self.session.instance.query(self.__class__)
        if limit:
            query = query.limit(limit)
        if offset:
            query = query.offset(offset)
        items = query.all()
        for item in items:
            result.append(
                MShopImage(
                    id=item.id,
                    item_id=item.item_id,
                    src=item.src
                )
            )
        return result

    def get_by_id(self, _id: int) -> MShopImage | None:
        item = self.session.instance.query(self.__class__).get(_id)
        if not item:
            return None
        return MShopImage(
            id=item.id,
            item_id=item.item_id,
            src=item.src
        )

    def get_by_item_id(self, item_id: int) -> list[MShopImage]:
        result = []
        items = self.session.instance.query(self.__class__)\
            .where(self.__class__.item_id == item_id).all()
        if not items:
            return result
        for item in items:
            result.append(
                MShopImage(
                    id=item.id,
                    item_id=item.item_id,
                    src=item.src
                )
            )
        return result
