from sqlalchemy.orm import Session as OrmSession


class Session:
    __connection__ = None
    __session__: OrmSession = None

    def __init__(self, connection):
        self.__connection__ = connection

    @property
    def instance(self) -> OrmSession:
        if self.__session__:
            self.__session__.close()
        self.__session__ = OrmSession(bind=self.__connection__)
        return self.__session__
