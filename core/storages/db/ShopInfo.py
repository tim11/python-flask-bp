from typing import Iterable

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, PrimaryKeyConstraint, Text, ForeignKey

from core.interfaces import IShopInfoStorage
from core.models import MShopInfo
from .Session import Session
from .Shop import Shop

Base = declarative_base()


class ShopInfo(Base, IShopInfoStorage):
    __tablename__ = 'shop_info'
    id = Column(Integer(), comment='Идентификатор', autoincrement=True)
    item_id = Column(Integer(), ForeignKey(
                         column=Shop.id,
                         onupdate='CASCADE',
                         ondelete='CASCADE'
                     ), comment='Идентификатор товар', nullable=False)
    description = Column(Text(), nullable=False, comment='Описание товара')

    __table_args__ = (
        PrimaryKeyConstraint('id', name='shop_info_pk'),
    )

    def __init__(self, session: Session):
        Base.__init__(self)
        IShopInfoStorage.__init__(self)
        self.session = session

    def get_all(self, limit: int = 0, offset: int = 0) -> Iterable[MShopInfo]:
        result = []
        query = self.session.instance.query(self.__class__)
        if limit:
            query = query.limit(limit)
        if offset:
            query = query.offset(offset)
        items = query.all()
        for item in items:
            result.append(
                MShopInfo(
                    id=item.id,
                    item_id=item.item_id,
                    description=item.description
                )
            )
        return result

    def get_by_id(self, _id: int) -> MShopInfo | None:
        item = self.session.instance.query(self.__class__).get(_id)
        if not item:
            return None
        return MShopInfo(
            id=item.id,
            item_id=item.item_id,
            description=item.description
        )

    def get_by_item_id(self, item_id: int) -> MShopInfo | None:
        item = self.session.instance.query(self.__class__)\
            .where(self.__class__.item_id == item_id).limit(1).one()
        if not item:
            return None
        return MShopInfo(
            id=item.id,
            item_id=item.item_id,
            description=item.description
        )
