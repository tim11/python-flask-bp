from sqlalchemy import create_engine


from core.interfaces import IStorage
from core.utils.Configurator import Configurator

from .Users import Users
from .Shop import Shop
from .ShopInfo import ShopInfo
from .ShopImage import ShopImage
from .Session import Session


class Storage(IStorage):
    def __init__(self, config: Configurator):
        login = config.get('mysql.user')
        password = config.get('mysql.password')
        host = config.get('mysql.host')
        port = config.get('mysql.port', 3306)
        database = config.get('mysql.database')
        dsn = f'mysql+pymysql://{login}:{password}@{host}:{port}/{database}'
        engine = create_engine(dsn)
        engine.connect()
        session = Session(engine)
        Users.metadata.create_all(engine)
        Shop.metadata.create_all(engine)
        ShopInfo.metadata.create_all(engine)
        ShopImage.metadata.create_all(engine)
        self.users = Users(session)
        self.shop = Shop(session)


