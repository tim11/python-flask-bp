from typing import Iterable

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, PrimaryKeyConstraint
from core.interfaces import IUsersStorage
from core.models import MUser
from .Session import Session

Base = declarative_base()


class Users(Base, IUsersStorage):
    __tablename__ = 'users'
    id = Column(Integer(), comment='Идентификатор', autoincrement=True)
    name = Column(String(100), nullable=False, comment='Имя пользователя')

    __table_args__ = (
        PrimaryKeyConstraint('id', name='users_pk'),
    )

    def __init__(self, session: Session):
        Base.__init__(self)
        IUsersStorage.__init__(self)
        self.session = session

    def get_all(self, limit: int = 0, offset: int = 0) -> Iterable[MUser]:
        result = []
        query = self.session.instance.query(self.__class__)
        if limit:
            query = query.limit(limit)
        if offset:
            query = query.offset(offset)
        items = query.all()
        for item in items:
            result.append(
                MUser(id=item.id, name=item.name)
            )
        return result

    def get_by_id(self, _id: int) -> MUser | None:
        item = self.session.instance.query(self.__class__).get(_id)
        if not item:
            return None
        return MUser(id=item.id, name=item.name)

