from web.modules.users import routes as route_users
from web.modules.shop import routes as route_shop

routes = {
    '/users': route_users,
    '/': route_shop
}

__all__ = ['routes']
