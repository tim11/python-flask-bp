from flask import Blueprint, abort, url_for
from web.main import app

_route = Blueprint('users', __name__,template_folder='templates')


@_route.route('/')
def index():
    users = app.storage.users.get_all()
    return f'''
    <ul>
        {''.join([f'<li>'
                  f'<a href="{url_for(".open", _id=item.id)}">'
                  f'{item.name}'
                  f'</a>'
                  f'</li>' for item in users
                  ])}
    </ul>
'''


@_route.route('/<_id>.html')
def open(_id: int):
    user = app.storage.users.get_by_id(_id)
    if not user:
        abort(404)
    return f'<h1>{user.name}</h1>'
