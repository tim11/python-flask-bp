from flask import Blueprint
from web.main import app

_route = Blueprint('shop', __name__,template_folder='templates')


@_route.route('/')
def index():
    item = app.storage.shop.get_by_id(1)
    test = ''
    print(item)
    for image in item.images:
        test += f'<img src="{image.src}"/>'
    return f'{item.name}<br/><p>{item.info.description}</p>{test}'


@_route.route('/test')
def test():
    return f'<h1>SHOP TEST</h1>'
