from flask import Flask
from core import Application as BaseApplication


class Application(BaseApplication):
    __flask__: Flask = None

    def __init__(self, config_file: str):
        super().__init__(config_file)
        self.__flask__ = Flask(__name__)

    def run(self):
        from web.modules import routes
        for prefix, route in routes.items():
            self.__flask__.register_blueprint(route, url_prefix=prefix)
        self.__flask__.run(
            host='127.0.0.1',
            port=4000,
            debug=1
        )


app = Application('./config.json')
if __name__ == '__main__':
    try:
        app.run()
    except BaseException as e:
        print(e)
